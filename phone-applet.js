// (function () {

const phoneAppletPrefixName = "ring_central";

const PHONE_FIELD_TYPES = [
    { field: 'work_phone', type: 'business' },
    { field: 'mobile_phone', type: 'mobile' },
    { field: 'alternate_phone_1', type: 'other' },
];

const phoneAppletFields = {
    from: `${phoneAppletPrefixName}_field0`,
    to: `${phoneAppletPrefixName}_field1`,
    contact: `${phoneAppletPrefixName}_field2`,
    duration: `${phoneAppletPrefixName}_field3`,
    direction: `${phoneAppletPrefixName}_field4`,
    note: `${phoneAppletPrefixName}_field5`,
    ringcentralId: `${phoneAppletPrefixName}_field6`,
    type: `${phoneAppletPrefixName}_field7`,
    readStatus: `${phoneAppletPrefixName}_field8`,
    messageStatus: `${phoneAppletPrefixName}_field9`,
    subject: `${phoneAppletPrefixName}_field10`,
    //status: `${phoneAppletPrefixName}_field6`,
}
