

let eventData = {};

var ffAAppletClient = null;

startupService();

function startupService() {
    ffAAppletClient = new FAAppletClient({
        appletId: APPLET_ID
    });

    notyf = new Notyf({
        duration: 20000,
        dismissible: true,
        position: {
            x: 'center',
            y: 'bottom',
        },
        types: [
            {
                type: 'info',
                className: 'info-notyf',
                icon: false,
            },
        ],
    });

    const clientId = RC_CLIENT_ID;
    const clientSecret = RC_CLIENT_SECRET;
    const redirectUri = RC_REDIRECT_URI;

    if (clientId && clientSecret) {
        const iFrame = document.createElement('iframe');
        const appServer = APP_SERVER_URL;
        iFrame.src = `https://ringcentral.github.io/ringcentral-embeddable/app.html?appKey=${clientId}&appSecret=${clientSecret}&appServer=${appServer}&redirectUri=${redirectUri}`;
        iFrame.style = 'width: 100%; height: 100%; border: none;';
        iFrame.allow = 'microphone';
        removeTextMessage();
        document.getElementById('frameContainer').appendChild(iFrame);

        const RING_CENTRAL_HANDLER = new RING_CENTRAL({
            RingCentral: iFrame.contentWindow,
            FAClient: ffAAppletClient,
            SERVICE_NAME: "FreeAgent App"
        });

        window.addEventListener('message', RING_CENTRAL_HANDLER.ringCentralListener.bind(RING_CENTRAL_HANDLER));
        console.log(RING_CENTRAL_HANDLER);
    }
}

function removeTextMessage() {
    const loadingText = document.getElementById('loading-text');
    loadingText.remove();
}

function cleanFooter() {
    const footer = document.getElementById('footer');
    footer.innerHTML = '';
}

